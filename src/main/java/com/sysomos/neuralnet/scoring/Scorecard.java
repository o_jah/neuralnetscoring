package com.sysomos.neuralnet.scoring;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.StringUtils;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.sysomos.neuralnet.scoring.utils.ScoringUtils;
import com.sysomos.word2vec.loader.WordVectorSerializer;
import com.sysomos.word2vec.word.vectors.WordVectors;

public class Scorecard {

	private Parameters params;

	public Scorecard(Parameters params) {
		this.params = params;
		if (params == null || params.model == null
				|| params.wordVectors == null)
			throw new RuntimeException(
					"Invalid parameters. Neural net couldn't be created based on inputted "
							+ "configs, weights, and serialized bigram net  files. Files cannot be found ");
	}

	public static class Word2VecLoader
			implements IStringConverter<WordVectors> {

		@Override
		public WordVectors convert(String path) {
			File file = new File(path);
			try {
				return WordVectorSerializer.loadTxtVectors(file);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public static class NeuralNetModelLoader
			implements IStringConverter<MultiLayerNetwork> {

		@Override
		public MultiLayerNetwork convert(String input) {
			try {
				if (StringUtils.isEmpty(input))
					throw new RuntimeException(
							"Model configs and weights paths cannot be found");

				String[] args = input.split(":");
				if (args == null || args.length < 2) {
					throw new RuntimeException(
							"One or the two expected arguments are missing");
				}
				String confFile = args[0];
				String coefFile = args[1];
				if (StringUtils.isEmpty(confFile)
						|| StringUtils.isEmpty(coefFile))
					throw new RuntimeException(
							"Model configs or weights paths cannot be found");

				MultiLayerConfiguration confFromJson = MultiLayerConfiguration
						.fromJson(
								FileUtils.readFileToString(new File(confFile)));
				DataInputStream dis = new DataInputStream(
						new FileInputStream(coefFile));
				INDArray newParams = Nd4j.read(dis);
				dis.close();
				MultiLayerNetwork model = new MultiLayerNetwork(confFromJson);
				model.init();
				model.setParams(newParams);
				return model;
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public static class Parameters {
		@Parameter(names = { "-model",
				"-m" }, required = false, description = "Neural network model: configs and weights "
						+ "format <config file (in json format)>:<weights file (in json format)>", converter = NeuralNetModelLoader.class)
		private MultiLayerNetwork model = null;
		@Parameter(names = { "-vocab",
				"-v" }, required = true, description = "Location of the bigram neural net. serialized "
						+ "model data", converter = Word2VecLoader.class)
		private WordVectors wordVectors;
	}

	/**
	 * Call this model to assign a class label to a user based on his/her
	 * collection of posts.
	 * 
	 * @param content
	 *            Collections of posts
	 * @return a class label
	 */
	public int fit(final Collection<String> content) {
		if (CollectionUtils.isEmpty(content))
			throw new RuntimeException("Content is empty or null.");
		INDArray projection = ScoringUtils.project(content, params.wordVectors);
		if (projection == null)
			throw new RuntimeException(
					"Projecting content to the Binet network returned null.");

		INDArray guess = params.model.output(projection);
		return ScoringUtils.getBestGuess(guess);
	}

	/**
	 * Call this model to assign class labels to a set of users based on their
	 * concatenated posts.
	 * 
	 * @param dataFile
	 *            File containing user IDs and posts where the posts are
	 *            concatenated.
	 * @param delimiter
	 *            Column delimiter in the file
	 * @return a map of class labels where the keys are user IDs and the values
	 *         are the class labels.
	 */
	public Map<String, Integer> fit(final File dataFile, String delimiter) {
		Multimap<String, String> data = ArrayListMultimap.create();
		try {
			BufferedInputStream bfs = new BufferedInputStream(
					new FileInputStream(dataFile));
			LineIterator iter = IOUtils.lineIterator(bfs, "UTF-8");
			if (iter == null)
				throw new RuntimeException("Couldn't load file.");
			while (iter.hasNext()) {
				String line = iter.next();
				if (StringUtils.isEmpty(line))
					continue;
				String tokens[] = line.split(delimiter);
				if (tokens == null || tokens.length < 2
						|| StringUtils.isEmpty(tokens[0])
						|| StringUtils.isEmpty(tokens[1]))
					continue;
				data.put(tokens[0], tokens[1]);
			}
		} catch (IOException e) {
			throw new RuntimeException(e.getLocalizedMessage(), e);
		}
		if (data.isEmpty()) {
			throw new RuntimeException("File is empty");
		}

		Map<String, Integer> results = new HashMap<String, Integer>();
		for (String key : data.keySet()) {
			INDArray projection = ScoringUtils.project(data.get(key),
					params.wordVectors);
			if (projection == null)
				continue;

			INDArray guess = params.model.output(projection);
			results.put(key, ScoringUtils.getBestGuess(guess));
		}
		return results;
	}

}
